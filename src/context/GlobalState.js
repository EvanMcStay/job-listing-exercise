import React, { createContext, useReducer } from "react";
import AppReducer from "./AppReducer";
import { v4 as uuid } from "uuid";

const initialState = {
  jobListings: [
    {
      id: uuid(),
      title: "Product Manager",
      location: "Austin, TX",
      posted: "10/04/2020",
      sponsorship: "Free",
      status: "Open",
      editAction: "Edit",
      deleteAction: "Delete"
    },
    {
      id: uuid(),
      title: "CEO",
      location: "Austin, TX",
      posted: "12/30/2020",
      sponsorship: "Sponsored",
      status: "Paused",
      editAction: "Edit",
      deleteAction: "Delete"
    },
    {
      id: uuid(),
      title: "Software Engineer",
      location: "Seattle, WA",
      posted: "11/18/2020",
      sponsorship: "Free",
      status: "Closed",
      editAction: "Edit",
      deleteAction: "Delete"
    }
  ]
};

export const GlobalContext = createContext(initialState);

export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  const createListing = (job) => {
    dispatch({
      type: "CREATE_LISTING",
      payload: job
    });
  };

  const updateListing = (job) => {
    dispatch({
      type: "UPDATE_LISTING",
      payload: job
    });
  };

  const deleteListing = (id) => {
    dispatch({
      type: "DELETE_LISTING",
      payload: id
    });
  };

  return (
    <GlobalContext.Provider
      value={{
        jobListings: state.jobListings,
        createListing,
        updateListing,
        deleteListing
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
