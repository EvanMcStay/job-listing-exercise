import React from "react";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import PropTypes from "prop-types";

// Styles
const labelStyles = css`
  font-family: "Helvetica", sans-serif;
  font-weight: bold;
  line-height: 150%;
  color: #2d2d2d;
`;
const helperTextStyles = css`
  font-family: "Helvetica", sans-serif;
  font-size: 14px;
  line-height: 150%;
  color: #767676;
`;
const input = css`
  font-size: 16px;
  color: #2d2d2d;
  padding: 12px;
  border: 1px solid #949494;
  border-radius: 8px;
  width: 100%;
  appearance: none;
  -webkit-appearance: none;
  -moz-appearance: none;

  &::placeholder {
    color: #767676;
  }
  &:focus {
    border-color: #085ff7;
    border-radius: 8px;
  }

  @media (min-width: 768px) {
    width: 300px;
  }
`;

// Component
const FormInput = ({
  id,
  label,
  helperText,
  type,
  name,
  value,
  placeholder,
  className,
  onFormInputChange,
  onFormInputBlur
}) => {
  return (
    <div>
      <label css={labelStyles} htmlFor={id}>
        {label}
      </label>
      <p css={helperTextStyles}>{helperText}</p>
      <input
        css={input}
        type={type}
        id={id}
        name={name}
        value={value}
        placeholder={placeholder}
        className={className}
        onChange={onFormInputChange}
        onBlur={onFormInputBlur}
      ></input>
    </div>
  );
};

FormInput.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.oneOf(["text", "tel", "number", "email", "password"])
    .isRequired,
  helperText: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  className: PropTypes.string,
  onFormInputChange: PropTypes.func,
  onFormInputBlur: PropTypes.func
};

export default FormInput;
