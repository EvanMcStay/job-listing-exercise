import React, { useContext } from "react";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Button from "../Button/Button";
import { GlobalContext } from "../../context/GlobalState";

// Styles
const table = css`
  width: 100%;
  font-family: "Noto Sans", sans-serif;
  border-spacing: 0 16px;
  border-collapse: separate;
`;
const tableHeadings = css`
  font-size: 14px;
  line-height: 150%;
  color: #949494;
  &:first-of-type {
    padding-left: 18px;
    text-align: left;
  }
`;
const tableBody = css`
  font-size: 14px;
  line-height: 150%;
  margin-bottom: 16px;

  & td {
    min-height: 82px;
    background-color: #ffffff;
    padding: 18px;
    border-spacing: 16px 0;
    text-align: center;
    border-top: 1px solid #d4d2d0;
    border-bottom: 1px solid #d4d2d0;

    &:first-of-type {
      display: flex;
      flex-direction: column;
      text-align: left;
      border-left: 1px solid #d4d2d0;
      border-top-left-radius: 8px;
      border-bottom-left-radius: 8px;
    }
    &:last-of-type {
      text-align: right;
      border-right: 1px solid #d4d2d0;
      border-top-right-radius: 8px;
      border-bottom-right-radius: 8px;
    }
  }
`;
const location = css`
  color: #949494;
`;
const buttonSpacer = css`
  margin-right: 16px;
`;

// Component
const Table = ({ headings, data }) => {

  const { deleteListing } = useContext(GlobalContext);

  const capitalizeText = (text) => {
    return `${text.charAt(0).toUpperCase()}${text.slice(1)}`;
  };

  const renderedHeadings = headings.map((heading, index) => {
    return (
      <th key={index} css={tableHeadings}>
        {heading}
      </th>
    );
  });

  const renderedBody = data.map((item, index) => {
    return (
      <tr key={index} css={tableBody}>
        <td>
          {item.title}
          <span css={location}>{item.location}</span>
        </td>
        <td>{item.posted}</td>
        <td>{capitalizeText(item.sponsorship)}</td>
        <td>{capitalizeText(item.status)}</td>
        <td>
          <Link to={`/edit/${item.id}`} css={buttonSpacer}>
            <Button variant="secondary" label={item.editAction} />
          </Link>
          <Button variant="secondary" label={item.deleteAction} onButtonClick={() => deleteListing(item.id)} />
        </td>
      </tr>
    );
  });

  return (
    <table css={table}>
      <thead>
        <tr>{renderedHeadings}</tr>
      </thead>
      <tbody>{renderedBody}</tbody>
    </table>
  );
};

Table.propTypes = {
  headings: PropTypes.arrayOf(PropTypes.string).isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Table;
