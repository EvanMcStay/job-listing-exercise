import React, { useState, useEffect, useContext } from "react";
import { v4 as uuid } from "uuid";
import { useHistory } from "react-router-dom";
import { GlobalContext } from "../../context/GlobalState";
import Card from "../../components/Card/Card";
import FormInput from "../../components/FormInput/FormInput";
import Select from "../../components/Select/Select";

const AddJob = () => {
  const [title, setTitle] = useState("");
  const [location, setLocation] = useState("");
  const [sponsorship, setSponsorship] = useState("");
  const [status, setStatus] = useState("");
  const [isActionDisabled, setIsActionDisabled] = useState(true);

  const sponsorshipOptions = [
    { label: "Free", value: "free" },
    { label: "Sponsored", value: "sponsored" }
  ];
  const statusOptions = [
    { label: "Open", value: "open" },
    { label: "Paused", value: "paused" },
    { label: "Closed", value: "closed" }
  ];

  const { createListing } = useContext(GlobalContext);

  const history = useHistory();

  useEffect(() => {
    if (title && location) {
      setIsActionDisabled(false);
    }
  }, [title, location]);

  const getTimestamp = () => {
    const today = new Date();
    const day = String(today.getDate()).padStart(2, "0");
    const month = String(today.getMonth() + 1).padStart(2, "0");
    const year = today.getFullYear();

    return `${month}/${day}/${year}`;
  };

  const onSubmit = (event) => {
    event.preventDefault();

    const newJobListing = {
      id: uuid(),
      title: title,
      location: location,
      posted: getTimestamp(),
      sponsorship: sponsorship,
      status: status,
      action: "Edit"
    };

    if (newJobListing.sponsorship === "") {
      newJobListing.sponsorship = sponsorshipOptions[0].value;
    }

    if (newJobListing.status === "") {
      newJobListing.status = statusOptions[0].value;
    }

    createListing(newJobListing);
    history.push("/");
  };

  const cardActions = [
    { label: "Cancel", variant: "secondary", route: "/" },
    {
      label: "Add job",
      type: "submit",
      variant: "primary",
      disabled: isActionDisabled,
      onButtonClick: onSubmit
    }
  ];

  return (
    <Card
      title="Add a new job"
      instructions="Fill out the information for your new job listing."
      actions={cardActions}
    >
      <form onSubmit={onSubmit}>
        <FormInput
          label="Job title"
          helperText="What is the name of the role?"
          id="job-title"
          type="text"
          className="margin-bottom"
          placeholder="e.g. Software Engineer"
          onFormInputBlur={(event) => setTitle(event.target.value)}
        />
        <FormInput
          label="Location"
          helperText="Where is this job?"
          id="location"
          type="text"
          className="margin-bottom"
          placeholder="e.g. Chicago, IL"
          onFormInputBlur={(event) => setLocation(event.target.value)}
        />
        <Select
          label="Sponsorship"
          helperText="Do you want to promote this job?"
          id="sponsorship-select"
          className="margin-bottom"
          options={sponsorshipOptions}
          defaultValue={sponsorshipOptions[0].value}
          onSelectChange={(event) => setSponsorship(event.target.value)}
        />
        <Select
          label="Status"
          helperText="Are you ready to make this job listing public?"
          id="status-select"
          options={statusOptions}
          defaultValue={statusOptions[0].value}
          onSelectChange={(event) => setStatus(event.target.value)}
        />
      </form>
    </Card>
  );
};

export default AddJob;
