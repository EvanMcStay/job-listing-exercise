# Jobs Listing Exercise

## Introduction

This project is the result of an Angular developer learning React over a few days and then creating a project based on the exercise design. I added to the original design in a Figma file that can be found [here](https://www.figma.com/file/lkWD94UHbj7hkVf612zJ83/Code-Exercise-Architecture?node-id=0%3A1). You can run the project locally or view the app with this link [https://evanmcstay.gitlab.io/job-listing-exercise](https://evanmcstay.gitlab.io/job-listing-exercise). I hope you like what you see!!!

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Application Structure

I split the application up into 3 main pages to handle the create, read, and update functionality. I chose to use emotion to style the project to get a feel for how it would be to style a component at Indeed. I initially chose to use `@emotion/styled` method but switched over to `@emotion/react` method. I handled the create and update functionality using the Context API since that is what I was most familiar with through my learning path of React. I also chose to create functional components over class-based components since I was more comfortable with functional components using React hooks.

## Component Architecture

I split the components up into 6 reusable components based on the exercise design. Below are the props that can be used for each component.

### Button Props

| Name          | Type     | Required | Description                                                                                                 |
| ------------- | -------- | -------- | ----------------------------------------------------------------------------------------------------------- |
| variant       | string   | Yes      | Defines the variant of button that will render to the screen. Accepted values are `primary` or `secondary`. |
| label         | string   | Yes      | Defines the label text for the button.                                                                      |
| type          | string   | Yes      | Defines the behavior type of the button. Accepted values are `submit`, `reset`, or `button`.                |
| name          | string   | No       | Defines the name of the button used with the `value` when submitting form data.                             |
| value         | string   | No       | Defines the value of the button associated with the `name` when submitting form data.                       |
| disabled      | boolean  | No       | Defines the disabled state of the button.                                                                   |
| onButtonClick | function | No       | Hooks to the `onClick` event fired inside the component.                                                    |

### Card Props

| Name         | Type             | Required | Description                                                                                       |
| ------------ | ---------------- | -------- | ------------------------------------------------------------------------------------------------- |
| children     | element          | Yes      | Defines the element(s) that will be rendered inside the card body.                                |
| title        | string           | No       | Defines the rendered title of the card                                                            |
| instructions | string           | No       | Defines the rendered instructions of the card. Used to guide the user through the card's content. |
| actions      | array of objects | No       | Defines the button actions for the card (e.g. Cancel or Submit).                                  |

#### `actions` Object Structure

| Property     | Type      | Description                                                                             |
| ------------ | --------- | --------------------------------------------------------------------------------------- |
| `<Button />` | component | Uses the `Button` component props for the action (`variant`, `label`, `disabled`, etc.) |
| route        | string    | Defines the navigational route for an action.                                           |

**Notes**: In the actual component, I wrote the functionality to conditionally show/hide the heading and/or footer. Since the card body accepts any element(s), the developer user has the flexibility to build virtually any card layout.

### FormInput Props

| Name              | Type     | Required | Description                                                                                                                                                   |
| ----------------- | -------- | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| id                | string   | Yes      | Defines the unique identifier for the form control. This prop is also used as the value for the `label`'s `for` attribute to link the control to the `label`. |
| label             | string   | Yes      | Defines the label text for the form control.                                                                                                                  |
| type              | string   | Yes      | Defines the type of form control to render. Accepted values are `text`, `tel`, `number`, `email`, and `password`                                              |
| helperText        | string   | No       | Defines the helper text for the form control.                                                                                                                 |
| name              | string   | No       | Defines the name of the form control used with the `value` when submitting form data                                                                          |
| value             | string   | No       | Defines the value of the form control.                                                                                                                        |
| placeholder       | string   | No       | Defines the placeholder of the form control. This is used to provide a hint for the kind of information the field is expecting.                               |
| className         | string   | No       | Defines the css class override(s) for the component.                                                                                                          |
| onFormInputChange | function | No       | Hooks to the `onChange` event fired inside the component.                                                                                                     |
| onFormInputBlue   | function | No       | Hooks to the `onBlur` event fired inside the component.                                                                                                       |

### Navbar Props

| Name     | Type   | Required | Description                                                     |
| -------- | ------ | -------- | --------------------------------------------------------------- |
| username | string | Yes      | Defines the user's name (e.g. David Bowie) for the application. |
| userRole | string | Yes      | Defines the user's role (e.g. Admin) within the application.    |

### Select Props

| Name           | Type             | Required | Description                                                                                                                                                       |
| -------------- | ---------------- | -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| id             | string           | Yes      | Defines the unique identifier for the form control. This prop is also used as the value for the `label`'s `for` attribute to link the control to the `label`.     |
| label          | string           | Yes      | Defines the label text for the form control.                                                                                                                      |
| options        | array of objects | Yes      | Defines the options that will display for the user to pick from.                                                                                                  |
| helperText     | string           | No       | Defines the helper text for the form control.                                                                                                                     |
| name           | string           | No       | Defines the name of the form control used with the `value` when submitting form data.                                                                             |
| value          | string           | No       | Defines the value of the form control.                                                                                                                            |
| defaultValue   | string           | No       | Defines the default `selected` value for an option. The `defaultValue` must match the `value` of the option the developer user wants to have selected by default. |
| className      | string           | No       | Defines the css class override(s) for the component.                                                                                                              |
| onSelectChange | function         | No       | Hooks to the `onChange` event fired inside the component.                                                                                                         |

#### `options` Object Structure

| Property | Type   | Description                                                                                        |
| -------- | ------ | -------------------------------------------------------------------------------------------------- |
| label    | string | The text that will be displayed for each option.                                                   |
| value    | string | The value for the option. This coordinates with the `defaultValue` to set an option as `selected`. |

### Table Props

| Name     | Type             | Required | Description                                |
| -------- | ---------------- | -------- | ------------------------------------------ |
| headings | array of strings | Yes      | Defines the column headings for the table. |
| data     | array of objects | Yes      | Defines the data for the table.            |

#### `data` Object Structure

| Property    | Type   | Description                                            |
| ----------- | ------ | ------------------------------------------------------ |
| id          | string | Sets the unique identifier for the job.                |
| title       | string | Sets the title for the job.                            |
| location    | string | Sets the location for the job.                         |
| posted      | string | Sets the date the job was posted.                      |
| sponsorship | string | Sets the sponsorship option for the job.               |
| status      | string | Sets the status option for the job.                    |
| action      | string | Sets the title for the action button within the table. |

**Notes**: This component can use the most enhancements to be truly 'reusable.' As it is now, the table layout is highly tied to the object structure which makes it only reusable for projects using the data structure instead of a general purpose table component.

## Exercise Retrospective

I am very appreciative of this exercise. I have _learning React_ on my learning plan for 2021 and this exercise fast tracked my learning efforts and focus. Though I did not get as far as writing the tests, I learned a lot of different paradigms and am happy with the outcome. I learned a lot about React but there is still more to learn as there always is. I believe that this exercise was a fair request to provide to a prospective candidate.
