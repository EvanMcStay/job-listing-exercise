import React, { useState, useEffect, useContext } from "react";
import { useHistory } from "react-router-dom";
import Card from "../../components/Card/Card";
import FormInput from "../../components/FormInput/FormInput";
import Select from "../../components/Select/Select";
import { GlobalContext } from "../../context/GlobalState";

const EditJob = (props) => {
  const [selectedListing, setSelectedListing] = useState({
    id: "",
    title: "",
    location: "",
    posted: "",
    sponsorship: "",
    status: ""
  });
  const [isActionDisabled, setIsActionDisabled] = useState(true);

  const sponsorshipOptions = [
    { label: "Free", value: "free", id: 0 },
    { label: "Sponsored", value: "sponsored", id: 1 }
  ];
  const statusOptions = [
    { label: "Open", value: "open", id: 0 },
    { label: "Paused", value: "paused", id: 1 },
    { label: "Closed", value: "closed", id: 2 }
  ];

  const { jobListings, updateListing } = useContext(GlobalContext);

  const history = useHistory();
  const selectedListingId = props.match.params.id;

  useEffect(() => {
    const listingId = selectedListingId;
    const selectedListing = jobListings.find(
      (listing) => listing.id === listingId
    );

    setSelectedListing(selectedListing);
  }, [selectedListingId, jobListings]);

  const getTimestamp = () => {
    const today = new Date();
    const day = String(today.getDate()).padStart(2, "0");
    const month = String(today.getMonth() + 1).padStart(2, "0");
    const year = today.getFullYear();

    return `${month}/${day}/${year}`;
  };

  const onSubmit = (event) => {
    event.preventDefault();

    selectedListing.posted = getTimestamp();
    updateListing(selectedListing);

    history.push("/");
  };

  const onFormChange = (event) => {
    setSelectedListing({
      ...selectedListing,
      [event.target.name]: event.target.value
    });

    setIsActionDisabled(false);
  };

  const cardActions = [
    { label: "Cancel", variant: "secondary", route: "/" },
    {
      label: "Save",
      type: "submit",
      variant: "primary",
      disabled: isActionDisabled,
      onButtonClick: onSubmit
    }
  ];

  return (
    <Card
      title="Edit job"
      instructions="Edit the information for your job listing."
      actions={cardActions}
    >
      <form onSubmit={onSubmit}>
        <FormInput
          label="Job title"
          helperText="What is the name of the role?"
          id="job-title"
          type="text"
          name="title"
          className="margin-bottom"
          value={selectedListing.title}
          placeholder="e.g. Software Engineer"
          onFormInputChange={onFormChange}
        />
        <FormInput
          label="Location"
          helperText="Where is this job?"
          id="location"
          type="text"
          name="location"
          className="margin-bottom"
          value={selectedListing.location}
          placeholder="e.g. Chicago, IL"
          onFormInputChange={onFormChange}
        />
        <Select
          label="Sponsorship"
          helperText="Do you want to promote this job?"
          id="sponsorship-select"
          name="sponsorship"
          className="margin-bottom"
          options={sponsorshipOptions}
          value={selectedListing.sponsorship}
          onSelectChange={onFormChange}
        />
        <Select
          label="Status"
          helperText="Are you ready to make this job listing public?"
          id="status-select"
          name="status"
          options={statusOptions}
          value={selectedListing.status}
          onSelectChange={onFormChange}
        />
      </form>
    </Card>
  );
};

export default EditJob;
