import React from "react";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import PropTypes from "prop-types";

// Styles
const labelStyles = css`
  font-family: "Helvetica", sans-serif;
  font-weight: bold;
  line-height: 150%;
  color: #2d2d2d;
`;
const helperTextStyles = css`
  font-family: "Helvetica", sans-serif;
  font-size: 14px;
  line-height: 150%;
  color: #767676;
`;
const select = css`
  font-size: 16px;
  background-color: #ffffff;
  color: #2d2d2d;
  padding: 12px;
  border: 1px solid #949494;
  border-radius: 8px;
  width: 100%;

  // Safari and Firefox select support
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  background: url("data:image/svg+xml,%3Csvg%20width%3D%2214%22%20height%3D%228%22%20viewBox%3D%220%200%2014%208%22%20fill%3D%22none%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0A%3Cpath%20d%3D%22M13.0015%201.88758C13.2017%201.6874%2013.2056%201.36684%2013.0104%201.17157L12.3033%200.464468C12.108%200.269206%2011.7874%200.273188%2011.5873%200.473362L6.99997%205.06066L2.41297%200.473667C2.21258%200.273277%201.89184%200.26912%201.69658%200.464382L0.989473%201.17149C0.794211%201.36675%200.798368%201.68749%200.998757%201.88788L6.64641%207.53554C6.74675%207.63587%206.88016%207.6836%207.01333%207.67913C7.13748%207.6768%207.25986%207.62902%207.35343%207.53545L7.35425%207.53463C7.35699%207.532%207.35972%207.52934%207.36241%207.52664L13.0015%201.88758Z%22%20fill%3D%22%232D2D2D%22%2F%3E%0A%3C%2Fsvg%3E%0A");
  background-repeat: no-repeat;
  background-position: center right 12px;

  &::placeholder {
    color: #767676;
  }
  &:focus {
    border: 1px solid #085ff7;
  }

  @media (min-width: 768px) {
    width: 300px;
  }
`;

// Component
const Select = ({
  id,
  label,
  helperText,
  name,
  value,
  options,
  className,
  defaultValue,
  onSelectChange
}) => {
  const renderedOptions = options.map((option, index) => {
    return (
      <option key={index} value={option.value}>
        {option.label}
      </option>
    );
  });

  return (
    <div>
      <label css={labelStyles} htmlFor={id}>
        {label}
      </label>
      <p css={helperTextStyles}>{helperText}</p>
      <select
        css={select}
        id={id}
        name={name}
        className={className}
        value={value}
        defaultValue={defaultValue}
        onChange={onSelectChange}
      >
        {renderedOptions}
      </select>
    </div>
  );
};

Select.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  helperText: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  defaultValue: PropTypes.string,
  className: PropTypes.string,
  onSelectChange: PropTypes.func
};

export default Select;
