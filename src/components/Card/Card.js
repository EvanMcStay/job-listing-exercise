import React from "react";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Button from "../Button/Button";

// Styles
const card = css`
  background-color: #ffffff;
  border-radius: 8px;
  border: 1px solid #d4d2d0;
  font-family: "Noto Sans", sans-serif;
  color: #000000;
  width: 100%;
  margin: 0;

  @media (min-width: 768px) {
    max-width: 600px;
    margin: 80px auto;
  }
  @media (min-width: 992px) {
    max-width: 800px;
  }
`;
const cardSection = css`
  padding: 1rem;

  &:nth-of-type(even) {
    border-top: 1px solid #d4d2d0;
  }

  @media (min-width: 768px) {
    padding: 2rem;
  }
`;
const cardTitle = css`
  font-size: 20px;
  font-weight: 700;
  line-height: 150%;
  color: #000000;
`;
const cardInstruction = css`
  line-height: 150%;
`;
const cardFooter = css`
  display: flex;
  align-items: center;
  justify-content: space-around;
  padding: 20px 1rem;
  border-top: 1px solid #d4d2d0;

  @media (min-width: 768px) {
    justify-content: flex-end;
    padding: 20px 2rem;
  }
`;
const buttonSpacer = css`
  margin-right: 16px;
`;

// Component
const Card = ({ title, instructions, actions, children }) => {
  let renderedButtons;

  if (actions) {
    renderedButtons = actions.map((button, index) => {
      return button.route ? (
        <Link to={`${button.route}`} css={buttonSpacer} key={index}>
          <Button variant={button.variant} label={button.label} />
        </Link>
      ) : (
        <Button
          key={index}
          variant={button.variant}
          label={button.label}
          type={button.type}
          disabled={button.disabled}
          onButtonClick={button.onButtonClick}
        />
      );
    });
  }

  return (
    <div css={card}>
      {(title || instructions) && (
        <div css={cardSection}>
          <h2 css={cardTitle}>{title}</h2>
          <p css={cardInstruction}>{instructions}</p>
        </div>
      )}
      <div css={cardSection}>{children}</div>
      {actions && <div css={cardFooter}>{renderedButtons}</div>}
    </div>
  );
};

Card.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
  instructions: PropTypes.string,
  actions: PropTypes.arrayOf(PropTypes.object)
};

export default Card;
