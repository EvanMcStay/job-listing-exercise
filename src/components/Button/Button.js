import React from "react";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import PropTypes from "prop-types";

// Styles
const theme = {
  blue: "#085ff7",
  blue20: "#064cc6",
  blue40: "#053994",
  blue60: "#9cbffc",
  white: "#ffffff",
  gray20: "#f8f8f8",
  gray40: "#f2f2f2",
  gray60: "#e8e8e8",
  gray80: "#767676",
  border: "#d4d2d0"
};
const button = css`
  height: 44px;
  padding: 0 16px;
  border-radius: 8px;
  font-size: 16px;
  font-weight: 600;
  font-family: "Helvetica", sans-serif;
  line-height: 112.5%;
  &:hover {
    cursor: pointer;
  }
  &:disabled {
    cursor: not-allowed;
  }
`;
const primary = css`
  ${button};
  color: ${theme.white};
  background-color: ${theme.blue};
  border: 1px solid ${theme.blue};
  &:hover,
  &:focus {
    background-color: ${theme.blue20};
  }
  &:hover {
    border-color: ${theme.blue20};
  }
  &:focus {
    border-color: ${theme.blue60};
  }
  &:active {
    background-color: ${theme.blue40};
    border-color: ${theme.blue40};
  }
  &:disabled {
    background-color: ${theme.blue60};
    border-color: ${theme.blue60};
  }
`;
const secondary = css`
  ${button};
  color: ${theme.blue};
  background-color: ${theme.white};
  border: 1px solid ${theme.border};
  &:hover,
  &:focus {
    background-color: ${theme.gray60};
  }
  &:focus {
    border: 1px solid ${theme.blue};
  }
  &:active {
    background-color: ${theme.gray40};
  }
  &:disabled {
    color: ${theme.gray80};
    background-color: ${theme.gray60};
  }
`;

// Component
const Button = ({
  variant,
  label,
  type,
  name,
  value,
  disabled,
  onButtonClick
}) => {
  return variant === "primary" ? (
    <button
      css={primary}
      disabled={disabled}
      value={value}
      name={name}
      type={type}
      onClick={onButtonClick}
    >
      {label}
    </button>
  ) : (
    <button
      css={secondary}
      disabled={disabled}
      value={value}
      name={name}
      type={type}
      onClick={onButtonClick}
    >
      {label}
    </button>
  );
};

Button.propTypes = {
  variant: PropTypes.oneOf(["primary", "secondary"]).isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.oneOf(["submit", "reset", "button"]),
  name: PropTypes.string,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  onButtonClick: PropTypes.func
};

export default Button;
