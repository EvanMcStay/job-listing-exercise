import React from "react";
import { Global, css } from "@emotion/react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Navbar from "./components/Navbar/Navbar";
import Home from "./pages/Home/Home";
import EditJob from "./pages/EditJob/EditJob";
import AddJob from "./pages/AddJob/AddJob";
import { GlobalProvider } from "./context/GlobalState";

const globalStyles = css`
  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    outline: none;
    -webkit-tap-highlight-color: #ffffff;
    -webkit-focus-ring-color: #ffffff;
  }

  body {
    margin: 0;
    background-color: #faf9f8;
    font-size: 16px;
  }

  // Utility style
  .margin-bottom {
    margin-bottom: 1rem;
  }

  main {
    margin: 1rem;
  }

  @media (min-width: 768px) {
    main {
      margin: 50px auto;
      width: 70%;
      max-width: 1100px;
    }
    .margin-bottom {
      margin-bottom: 2rem;
    }
  }
`;

const App = () => {
  return (
    <React.Fragment>
      <Global styles={globalStyles} />
      <Navbar username={"Julie Howard"} userRole={"Admin"} />
      <main>
        <GlobalProvider>
          <Router>
            <Switch>
              <Route path="/" component={Home} exact />
              <Route path="/add" component={AddJob} exact />
              <Route path="/edit/:id" component={EditJob} exact />
              <Route path="*">
                <Redirect to="/" />
              </Route>
            </Switch>
          </Router>
        </GlobalProvider>
      </main>
    </React.Fragment>
  );
};

export default App;
