const AppReducer = (state, action) => {
  switch (action.type) {
    case "CREATE_LISTING":
      return {
        jobListings: [action.payload, ...state.jobListings]
      };
    case "UPDATE_LISTING":
      const updatedListing = action.payload;
      const updatedListings = state.jobListings.map((job) => {
        if (job.id === updatedListing.id) {
          return updatedListing;
        }
        return job;
      });
      return {
        jobListings: updatedListings
      };
    case "DELETE_LISTING":
      return {
        jobListings: state.jobListings.filter((job) => {
          return job.id !== action.payload;
        })
      };
    default:
      return state;
  }
};

export default AppReducer;
