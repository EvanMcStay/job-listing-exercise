import React from "react";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import PropTypes from "prop-types";

// Styles
const nav = css`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  background-color: #2d2d2d;
  padding: 0 20px;
  height: 64px;
`;
const ellipse = css`
  height: 40px;
  width: 40px;
  border-radius: 50%;
  background-color: #c4c4c4;
  margin-right: 10px;
`;
const userProfile = css`
  display: flex;
  flex-direction: column;
`;
const usernameStyle = css`
  margin: 0;
  color: #faf9f8;
  font-family: "Helvetica", sans-serif;
  font-weight: bold;
  line-height: 150%;
`;
const userRoleStyle = css`
  margin: 0;
  color: #faf9f8;
  font-size: 12px;
  font-family: "Helvetica", sans-serif;
  line-height: 150%;
`;

// Component
const Navbar = ({ username, userRole }) => {
  return (
    <header>
      <nav css={nav}>
        <div css={ellipse}></div>
        <div css={userProfile}>
          <p css={usernameStyle}>{username}</p>
          <p css={userRoleStyle}>{userRole}</p>
        </div>
      </nav>
    </header>
  );
};

Navbar.propTypes = {
  username: PropTypes.string.isRequired,
  userRole: PropTypes.string.isRequired
};

export default Navbar;
