import React, { useContext } from "react";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { Link } from "react-router-dom";
import Button from "../../components/Button/Button";
import Table from "../../components/Table/Table";
import { GlobalContext } from "../../context/GlobalState";

// Styles
const titleSection = css`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 2rem;
`;
const title = css`
  font-family: "Noto Sans", sans-serif;
  font-size: 28px;
  font-weight: 700;
  line-height: 125%;
  color: #2d2d2d;
  display: inline-block;
`;
const subtitle = css`
  font-family: "Noto Sans", sans-serif;
  font-size: 14px;
  font-weight: 400;
  line-height: 150%;
  color: #949494;
  margin-left: 10px;
  display: inline-block;
`;
const overflowTable = css`
  @media (max-width: 540px) {
    overflow-x: auto;
  }
`;

// Component
const Home = () => {
  const listings = useContext(GlobalContext);

  const tableConfig = {
    headings: ["Job Title", "Posted", "Sponsorship", "Status"],
    data: listings.jobListings
  };

  return (
    <React.Fragment>
      <section css={titleSection}>
        <div>
          <h1 css={title}>Jobs</h1>
          <p css={subtitle}>{listings.jobListings.length} listings</p>
        </div>
        <Link to="/add">
          <Button variant="primary" label="Add job" />
        </Link>
      </section>
      <section css={overflowTable}>
        <Table headings={tableConfig.headings} data={tableConfig.data} />
      </section>
    </React.Fragment>
  );
};

export default Home;
